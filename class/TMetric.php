<?php

/*
 * create class for getting tmetric date
 */
class TimeMetric {

  // get account token
  public function token() {
    $yaml         = file('access/auth.yaml');
    $yamlToken    = $yaml['10'];
    $stringArray  = explode(": ", $yamlToken);
    $token        = $stringArray[1];
    return $token;
  }

  //  rest api GET request
  public function getTmetricInfo($token, $get_path) {
    //    header('Content-Type: application/json'); // Specify the type of data
    header('Content-Type: text/html; charset=utf-8'); // Specify the type of data
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, "https://app.tmetric.com/api/" . $get_path);
    $authorization = 'Authorization: Bearer ' . $token; // Prepare the authorisation token
    curl_setopt($ch, CURLOPT_HTTPHEADER, [
      'Content-Type: application/json',
      $authorization
    ]); // Inject the token into the header
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
    $userProfileArray = curl_exec($ch); // Execute the cURL statement
    curl_close($ch); // Close the cURL connection
    return json_decode($userProfileArray, 1); // Return the received data
  }

  // rest api POST request
  public function postTmetricInfo($token, $data, $post_path) {
    header('Content-Type: text/html; charset=utf-8'); // Specify the type of data
    $ch = curl_init('https://app.tmetric.com/api/' . $post_path); // Initialise cURL

    $data          = json_encode($data); // Encode the data array into a JSON string
    $authorization = 'Authorization: Bearer ' . $token; // Prepare the authorisation token
    curl_setopt($ch, CURLOPT_HTTPHEADER, ['Content-Type: application/json', $authorization]); // Inject the token into the header
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
    curl_setopt($ch, CURLOPT_POST, 1); // Specify the request method as POST
    curl_setopt($ch, CURLOPT_POSTFIELDS, $data); // Set the posted fields
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1); // This will follow any redirects
    $result = curl_exec($ch); // Execute the cURL statement
    curl_close($ch); // Close the cURL connection
    return json_decode($result); // Return the received data
  }

  public $userProfile = 'userprofile';

  // get profile info
  public function profileInfo() {
    $profileInfo = $this->getTmetricInfo($this->token(), $this->userProfile);
    return $profileInfo;
  }
  // get path for GET requests
  function getPathInfo() {
    $getInfo        = $this->profileInfo();
    $userAccountId = $getInfo['activeAccountId'];
    $userProfileId = $getInfo['userProfileId'];
    $getPathArray = [
      'accountInfo'       => 'accounts/' . $userAccountId,
      'accountScopeInfo' => 'accounts/' . $userAccountId . '/scope',
      'projectsInfo'      => 'accounts/' . $userAccountId . '/projects'
    ];
    return $getPathArray;
  }

  // path for POST requests
  function postPathInfo() {
    $getInfo        = $this->profileInfo();
    $userAccountId = $getInfo['activeAccountId'];
    $userProfileId = $getInfo['userProfileId'];
    $postPathArray = [
      'postTimeEntries' => 'accounts/' . $userAccountId . '/timeentries/'
    ];
    return $postPathArray;
  }

  // get account info
  public function accountInfo() {
    $path        = $this->getPathInfo();
    $accountInfo = $this->getTmetricInfo($this->token(), $path['accountInfo']);
    return $accountInfo;
  }

  // get projects info
  public function projectInfo() {
    $path        = $this->getPathInfo();
    $projectInfo = $this->getTmetricInfo($this->token(), $path['projectsInfo']);
    return $projectInfo;
  }

  // for sending timeentries into tmetric
  public function postTimeEntries($startProject, $endProject, $projectName, $projectId, $description, $memberId) {
    $data        = [
      "endTime"         => $endProject,
      "startTime"       => $startProject,
      "isBillable"      => FALSE,
      "isInvoiced"      => FALSE,
      //      "timeEntryId"     => rand(1,777777),
      "timeEntryId"     => 0,
      "details"         => [
        "description" => $description,
        "projectId"   => $projectId
      ],
      "projectName"     => $projectName,
      "tagsIdentifiers" => []
    ];
    $path        = $this->postPathInfo();
    $memberTime = $this->postTmetricInfo($this->token(), $data, $path['postTimeEntries'] . $memberId);
  }
}
//=========end class============