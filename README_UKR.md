===================================UKRAINIAN===============================================

// коментарі

1) клас з усіма функціями знаходиться в config_tmetric.php

2) для роботи з tmetric function існує два типи запитів GET - для отримання інформації і POST - для її відправки.
 
Функції "getTmetricInfo()" та "postTmetricInfo()" відповідно.
обидві функції зсилаються на адресу сервера по 'https://app.tmetric.com/api/'.
Для формування повного запиту дані функції також отримують token з yaml в каталозі "/access"
за допомогою функції "token()" та передають його в свої header.
Для отримання конекретної інформації функіям потрібно передати повний шлях https запиту, наприклад:
'https://app.tmetric.com/api/' + 'accounts/' = 'https://app.tmetric.com/api/accounts/' - виведе інформацію про акаунт.

Необхідні теки для отримання інформації знаходяться у функціях  "getPathInfo()" та "postPathInfo()".
дані функції формують масиви шляхів ($getPathArray[], $postPathArray[]) для отримання інформації з використанням accountid користувача котрому належить токен.
    
    1)=================
    
    $getPathArray = [
      'accountInfo'       => 'accounts/' . $userAccountId, // доступ до інформації про акаунт
      'accountScopeInfo' => 'accounts/' . $userAccountId . '/scope', // розширена інформація про акаунт
      'projectsInfo'      => 'accounts/' . $userAccountId . '/projects' // інформація про проекти
    ];
    
    2)=================
    
    $postPathArray = [
      'postTimeEntries' => 'accounts/' . $userAccountId . '/timeentries/' // шлях для формування завдання на часовій відмітці
    ];
    
Ці функції можна розширювати, додаючи нові шляхи для отримання різної інформації у масиви.
Приклади запитів API rest знаходяться у "examples".

На основі "getPathInfo()" та "postPathInfo()" функцій є готові функції, що використовують їх
для формування повного шляху https, для доступу до інформації.
   Наприклад:
  
    public function accountInfo() {
        $path        = $this->getPathInfo(); // отримуєш масив шляхів
        $accountInfo = $this->getTmetricInfo($this->token(), $path['accountInfo']); // інформація про акаунт 
        return $accountInfo;
    }
     
Спочатку в $path передається виклик функції getPathInfo(), яка містить масив шляхів https, для отримання інформації.
Далі, в $accountInfo передається виклик getTmetricInfo(), котра використовує параметри token()
та повний шлях https, отримує інформацію та декодує з json в масив.
return $accountInfo повертає отриману інформацію функції.

Використання:
Для використання достатньо унаслідувати клас TimeMetric, 

    $getData  = new TimeMetric();

Та використовувати необхідні методи, 

    $accountInfo = $getData->accountInfo(); // виводить ынформацыю про акаунт
    
    $path = $getData->getPathInfo();
    $accountInfo = $getData->getTmetricInfo($getData->token(), $path['accountInfo']);
    
     /*
     *** OTHER EXAMPLES OF USING FUNCTIONS ***
     *
     * $profileInfo = $getData->profileInfo(); // отримує інформацію про профіль акаунта
     * $path        = $getData->getPathInfo(); // отримує інформацію про усі шляхи в getPathInfo() для get запитів
     */

Метод postTimeEntries() відправляє часові відмітки timeentries по проектам з csv файлу "csv/tmetric.csv" у
програму tmetric. Функція використовує вище описаний алгоритм для формування https шляху, 
та власні параметри, що визначені складом інформації у файлі "csv/tmetric.csv".

    postTimeEntries($startProject, $endProject, $projectName, $projectId, $description, $memberId)

    $startProject - час початку таймера.
    $endProject - час закінчення таймера.
    $projectName - назва проекта.
    $projectId - номер проекта.
    $description - опис до завдання.
    $memberId - номер користувача, котрому буде встановлено таймер.

Таким чином відправляються таймери в залежності від їх кількості у файлі "csv/tmetric.csv". Скрипт сам дістає імена
всіх користувачів проекту та їхні номери, передає їх у html для можливості вибору різних користувачів.
Скрипт при відправці переводить час в UTC формат,  звіряє чи існують вказані у файлі проекти та вибрані користувачі.
Далі скрипт передає всі дані у функцію для відправки на часову шкалу обраного користувача:
   
$postData     = new TimeMetric();

$postTimer = $postData->postTimeEntries($utcStartTime, $utcEndTime, $projectName, $projectId, $description, $memberId);

