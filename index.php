<!DOCTYPE html>
<html lang="ru">
<?php

// include Class
include "class/TMetric.php";
error_reporting(0);

// function create one array using array(keys)+array(values)
function combineArray(&$row, $key, $header) {
  $row = array_combine($header, $row); // use _combine_array for every array item
}

// load and parsing csv file
$fileName = $name = "tmetric";
if (!file_exists("csv/{$fileName}.csv")) {
  $fileName = $name;
  echo "<b class='allert'>File {$fileName}.csv isn't already exists in path /csv!</b>";
  return FALSE;
} else {
$csvArray = array_map('str_getcsv', file('csv/tmetric.csv')); // use str_getcsv for every elements in using arrays
$key       = array_keys($csvArray); // return array keys
$header    = array_shift($csvArray); // -extract first array value, next value will be [0]
array_walk($csvArray, 'combineArray', $header); // use _combine_array for every array item
}

// call class
$getData  = new TimeMetric();
// use function accountInfo() to get account information
$accountInfo = $getData->accountInfo();

// creating user_members array with names and ids
for ($i=0; $i<count($accountInfo['members']); $i++) {
  $userMemberId = $accountInfo['members'][$i]['userProfileId'];
  $userMemberName = $accountInfo['members'][$i]['userName'];
  $userMembers[$userMemberName] = [$userMemberId => $userMemberName];
}

?>
<head>
    <meta charset="UTF-8">
    <title>TMetric</title>
    <link rel="stylesheet" type="text/css" href="css/style.css">
</head>
<body>

<?php

// enter HTML
$n=0;
echo "<form method='post' name='times' class='times'>
  <button name='save-times' class='save-times' >Отправить данные</button>";
$set = 1;
foreach($csvArray as $row => $data) {

  // get row data
  if (!empty($data)) {
    $project = $header[0];
    $name = $data[$header[0]];
    $startProjectTime = $header[1];
    $start = $data[$header[1]];
    $endProjectTime = $header[2];
    $end = $data[$header[2]];
    $description = $header[3];
    $text = $data[$header[3]];

    $csvRow = $row+1;

    // display data
    echo "<fieldset>";

    // entering project array with names and values
    $p = 1;
    echo 'Row ' . $csvRow . " => " . $project . ": " . $name . '<br />';
    echo 'Row ' . $csvRow . " => " . $startProjectTime . ": " . $start . '<br />';
    echo 'Row ' . $csvRow . " => " . $endProjectTime . ": " . $end . '<br />';
    echo 'Row ' . $csvRow . " => " . $description . ": " . $text . '<br />';

    // create select with tmetric members
    echo "<p><select  name='select$set'>
            <option selected disabled>users:</option>";
    $m = 1;
    foreach ($userMembers as $key=>$member) {
      $memberId = array_key_first($member);
      $memberName = $member[$memberId];
      $members[] = $memberName;
      echo "<option name='member$m' id='member$m' class='member$m'>$memberName";
      $m++;
    }
    echo "</option>
            </select></p>
            </fieldset>";
    $set++;
  }
}
echo "</form>";
?>

<?php

// sending data to TMetric for creating timeentries
if(isset($_POST['save-times'])) {

  // create data for each row in CSV file
  $sel = 1;
  foreach ($csvArray as $row => $data) {

    // getting $_POST[select]
    $select = htmlspecialchars ($_POST["select$sel"]);
    if (empty($select)) {
        echo "<b class='allert'>You must select all users!</b>";
        return FALSE;
    }

    $project = $header[0];
    $name = $data[$header[0]];
    $startProjectTime = $header[1];
    $start = $data[$header[1]];
    $endProjectTime = $header[2];
    $end = $data[$header[2]];
    $text = $header[3];
    $description = $data[$header[3]];

    // convert time to UTC
    $startTime    =  new DateTime($start);
    $utcStartTime = $startTime->format("Y-m-d\TH:i:s");
    $endTime      = new DateTime($end);
    $utcEndTime   = $endTime->format('Y-m-d\TH:i:s');
    $projects     = $getData->projectInfo();

    // cheking name of project in the file and received array
    $pr = 0;
    foreach ($projects as $key=>$prj) {
      $projectName = $prj['projectName'];

      // if ($projectName == $name && $projectName == $select) {
      if ($projectName == $name) {
        $projectId = $prj['projectId'];

        // cheking member name from array with selected member name on page
        foreach ($userMembers as $item=>$member) {
          if ($item == $select) {
            $memberId = array_key_first($member);
          }
        }

        // sending POST request for adding timeentries
        $postData     = new TimeMetric();
        $postTimer = $postData->postTimeEntries($utcStartTime, $utcEndTime, $projectName, $projectId, $description, $memberId);
      } else {
          echo "<b class='allert'>This project '$name' isn't already exists!</b>";
//          return FALSE;
      }
    }
    $sel++;
  }
}

?>

</body>
</html>
