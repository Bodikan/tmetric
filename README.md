===================================UKRAINIAN===============================================

// коментарі

1) клас з усіма функціями знаходиться в config_tmetric.php

2) для роботи з tmetric function існує два типи запитів GET - для отримання інформації і POST - для її відправки.
 
Функції "getTmetricInfo()" та "postTmetricInfo()" відповідно.
обидві функції зсилаються на адресу сервера по 'https://app.tmetric.com/api/'.
Для формування повного запиту дані функції також отримують token з yaml в каталозі "/access"
за допомогою функції "token()" та передають його в свої header.
Для отримання конекретної інформації функіям потрібно передати повний шлях https запиту, наприклад:
'https://app.tmetric.com/api/' + 'accounts/' = 'https://app.tmetric.com/api/accounts/' - виведе інформацію про акаунт.

Необхідні теки для отримання інформації знаходяться у функціях  "getPathInfo()" та "postPathInfo()".
дані функції формують масиви шляхів ($getPathArray[], $postPathArray[]) для отримання інформації з використанням accountid користувача котрому належить токен.
    
    1)=================
    
    $getPathArray = [
      'accountInfo'       => 'accounts/' . $userAccountId, // доступ до інформації про акаунт
      'accountScopeInfo' => 'accounts/' . $userAccountId . '/scope', // розширена інформація про акаунт
      'projectsInfo'      => 'accounts/' . $userAccountId . '/projects' // інформація про проекти
    ];
    
    2)=================
    
    $postPathArray = [
      'postTimeEntries' => 'accounts/' . $userAccountId . '/timeentries/' // шлях для формування завдання на часовій відмітці
    ];
    
Ці функції можна розширювати, додаючи нові шляхи для отримання різної інформації у масиви.
Приклади запитів API rest знаходяться у "examples".

На основі "getPathInfo()" та "postPathInfo()" функцій є готові функції, що використовують їх
для формування повного шляху https, для доступу до інформації.
   Наприклад:
  
    public function accountInfo() {
        $path        = $this->getPathInfo(); // отримуєш масив шляхів
        $accountInfo = $this->getTmetricInfo($this->token(), $path['accountInfo']); // інформація про акаунт 
        return $accountInfo;
    }
     
Спочатку в $path передається виклик функції getPathInfo(), яка містить масив шляхів https, для отримання інформації.
Далі, в $accountInfo передається виклик getTmetricInfo(), котра використовує параметри token()
та повний шлях https, отримує інформацію та декодує з json в масив.
return $accountInfo повертає отриману інформацію функції.

Використання:
Для використання достатньо унаслідувати клас TimeMetric, 

    $getData  = new TimeMetric();

Та використовувати необхідні методи, 

    $accountInfo = $getData->accountInfo(); // виводить ынформацыю про акаунт
    
    $path = $getData->getPathInfo();
    $accountInfo = $getData->getTmetricInfo($getData->token(), $path['accountInfo']);
    
     /*
     *** OTHER EXAMPLES OF USING FUNCTIONS ***
     *
     * $profileInfo = $getData->profileInfo(); // отримує інформацію про профіль акаунта
     * $path        = $getData->getPathInfo(); // отримує інформацію про усі шляхи в getPathInfo() для get запитів
     */

Метод postTimeEntries() відправляє часові відмітки timeentries по проектам з csv файлу "csv/tmetric.csv" у
програму tmetric. Функція використовує вище описаний алгоритм для формування https шляху, 
та власні параметри, що визначені складом інформації у файлі "csv/tmetric.csv".

    postTimeEntries($startProject, $endProject, $projectName, $projectId, $description, $memberId)

    $startProject - час початку таймера.
    $endProject - час закінчення таймера.
    $projectName - назва проекта.
    $projectId - номер проекта.
    $description - опис до завдання.
    $memberId - номер користувача, котрому буде встановлено таймер.

Таким чином відправляються таймери в залежності від їх кількості у файлі "csv/tmetric.csv". Скрипт сам дістає імена
всіх користувачів проекту та їхні номери, передає їх у html для можливості вибору різних користувачів.
Скрипт при відправці переводить час в UTC формат,  звіряє чи існують вказані у файлі проекти та вибрані користувачі.
Далі скрипт передає всі дані у функцію для відправки на часову шкалу обраного користувача:
   
$postData     = new TimeMetric();

$postTimer = $postData->postTimeEntries($utcStartTime, $utcEndTime, $projectName, $projectId, $description, $memberId);

=================================== ENGLISH ===============================================
 
 // comments
 
 1) a class with all functions is in config_tmetric.php
 
 2) There are two types of GET requests to work with tmetric function - to get information
 and POST to send it.
 
 The functions "getTmetricInfo()" and "postTmetricInfo()" respectively.
 both functions are referred to the server address at 'https://app.tmetric.com/api/'.
 To form a complete query, these functions also receive a token from yaml in the / access directory
 using the "token ()" function and pass it to their header.
 Functional information requires the complete https query path, such as:
 'https://app.tmetric.com/api/' + 'accounts /' = 'https://app.tmetric.com/api/accounts/' - displays account information.
 
 The required folders for getting information are in the "getPathInfo()" and "postPathInfo()" functions.
 these functions form path arrays ($get_path_array[], $post_path_array[]) to retrieve information using the accountid of the token-owned user.
     
    1) ==================
    
    $getPathArray = [
      'accountInfo' => 'accounts /'. $userAccountId, // access to information about account
      'accountScopeInfo' => 'accounts /'. $userAccountId. '/ scope', // extended information about account
      'projectsInfo' => 'accounts /'. $userAccountId. '/ projects' // information about projects
    ];
    
    2) ==================
    
    $postPathArray = [
      'postTimeEntries' => 'accounts /'. $userAccountId. '/ timeentries /' // hats for form filling in on watch
    ];
     
 These features can be expanded by adding new ways to get different information into arrays.
 Examples of restriction API requests are found in "examples".
 
 Based on the "getPathInfo()" and "postPathInfo()" functions, there are ready-made functions that use them
 to form a complete https path, to access information.
    Example:
   
    public function accountInfo() {
        $path = $this-> getPathInfo(); // otrimuєsh masiv shlyakhіv
        $accountInfo = $this-> getTmetricInfo ($this-> token(), $path ['accountInfo']); // Information about account
        return $accountInfo;
    }
   
 Initially, a call to the getPathInfo() function, which contains an array of https paths, is sent to $path for information.
 Next, a call to getTmetricInfo() that uses the token() parameters is passed to $accountInfo
 and full https path, retrieves information and decodes from json to array.
 return $accountInfo returns the function information received.
 
 Using:
 Just use the TimeMetric class to use,
 
    $getData = new TimeMetric();
 
 And use the necessary methods,
 
    $accountInfo = $getData-> accountInfo(); // enter information about account
    
    $path = $getData-> getPathInfo();
    $accountInfo = $getData-> getTmetricInfo ($getData-> token(), $path ['accountInfo']);
    
     / *
     *** OTHER EXAMPLES OF USING FUNCTIONS ***
     *
     * $profileInfo = $getData-> profileInfo(); // I will cancel information about the account profile
     * $path = $getData-> getPathInfo(); // I will cancel information about us hats in getPathInfo() for get feeds
     * /
 
 The postTimeEntries() method sends timeentries timestamps on projects from the csv file "csv / tmetric.csv" to
 tmetric program. Funсtion uses the algorithm described above to form the https path,
 and custom parameters defined by the composition of the information in the "csv / tmetric.csv" file.
 
    postTimeEntries ($startProject, $endProject, $projectName, $projectId, $description, $memberId)

    $startProject - hour to start the timer.
    $endProject - hour of the end of the timer.
    $projectName is the name of the project.
    $projectId is the project number.
    $description - Description until the next day.
    $memberId - the number of the koristuvach who will have a timer set.
 
 This sends the timers depending on their number in the "csv / tmetric.csv" file. The script itself gets the names
 all project users and their numbers, passes them in html for the choice of different users.
 The submit script translates the time into UTC format, verifies that the projects and selected users exist in the file.
 The script then sends all the data to a function to send to the timeline of the selected user:
    
$postData = new TimeMetric();

$postTimer = $postData-> postTimeEntries ($utcStartTime, $utcEndTime, $projectName, $projectId, $description, $memberId);